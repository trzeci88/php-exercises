<?php
/**
 * Created by PhpStorm.
 * User: Domino
 * Date: 11.10.2018
 * Time: 14:39
 */


if ($_SERVER['REQUEST_METHOD'] === "POST") {
    if (!empty($_POST['number1']) && !empty($_POST['number2']) && !empty($_POST['number3']) && !empty($_POST['number4']) &&
        !empty($_POST['number5']) && !empty($_POST['number6'])) {

        $numbers = [
            $_POST['number1'],
            $_POST['number2'],
            $_POST['number3'],
            $_POST['number4'],
            $_POST['number5'],
            $_POST['number6']
        ];

        //Wyświetlenie tablicy z wybranymi numerami
        echo "Wybrane liczby to : <br>";

        foreach ($numbers as $number) {
            echo "$number ";
        }

        $drawn_numbers = [];
        $i = 1;
        while ($i <= 6) {
            $random_number = mt_rand(1, 49);
            if (!in_array($random_number, $drawn_numbers)) {
                array_push($drawn_numbers, $random_number);
                $i++;
            }
        }
        echo "<br><br>Wylosowane liczby to : <br>";
        foreach ($drawn_numbers as $drawn_number) {
            echo "$drawn_number ";
        }


        $result = array_intersect($drawn_numbers, $numbers);

        if (count($result) == 0) {
            echo "<br><br> Nie trafiłeś żadnej liczby";
        } else {
            if (count($result) == 1) {
                echo "<br><br>Trafiłeś jedną liczbę :";
            } else {
                echo "<br><br>Trafiłeś następujące liczby :";
            }
        }

        foreach ($result as $res) {
            echo "<br>$res";
        }
    }
}
?>