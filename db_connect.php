<?php
/**
 * Created by PhpStorm.
 * User: Domino
 * Date: 18.10.2018
 * Time: 01:50
 */
$host = "localhost";
$user = "root";
$pass = "coderslab";
$db = "cinemas_ex";

$conn = new PDO("mysql:host=$host;dbname=$db;charset=utf8", $user, $pass);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$conn->setAtrribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);