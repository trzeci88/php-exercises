<?php

abstract class User
{
    protected $username;
    protected $password;
    private $loginCounter;

    abstract protected function checkLogin($login, $haslo);

    abstract protected function setLogin($login);

    abstract protected function setPassword($haslo);

    final public function login($username, $password)
    {
        if ($this->loginCounter >2) {
            return false;
        }

        $result = $this->checkLogin($username, $password);

        if ($result == false) {
            $this->loginCounter++;
            return false;
        } else {
            $this->loginCounter = 0;
            return true;
        }
    }
}