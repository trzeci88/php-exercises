<?php
include_once 'User.php';

class Admin extends User
{
    public function __construct($username, $password)
    {
        $this->setLogin($username);
        $this->setPassword($password);
    }

    protected function checkLogin($login, $haslo)
    {
        if ($this->username != $login) {
            return false;
        }
        if ($this->password != $haslo) {
            return false;
        }
        return true;
    }

    protected function setLogin($login)
    {
        $this->username = $login;
    }

    protected function setPassword($haslo)
    {
        if (strlen($haslo) > 9){
            $this->password = $haslo;
        } else {
            die('nie ma 10 znaków');
        }
    }
}