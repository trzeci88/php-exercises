<?php

class File
{
    protected $path;
    protected $size;

    public function getPath()
    {
        return $this->path;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        if ($size < 0 || !is_numeric($size)) {
            return false;
        }
        $this->size = $size;
    }

    public function setPath($path)
    {
        if (!is_string($path) || $path[0] !== '/') {
            return false;
        }
        $this->path = $path;
    }

    public function __construct($path, $size)
    {
        if (!is_string($path) || $path[0] !== '/' || $size < 0 || !is_numeric($size)) {
            return false;
        }

        $this->size = $size;
        $this->path = $path;
    }
    /**
     * Metoda obliczająca wagę pliku
     */
    public function calculateSize($unit)
    {
        switch ($unit) {
            case 'MB': {
                $result = floor($this->size / 1024 / 1024) . " MB";
                break;
            }
            case 'KB': {
                $result = floor($this->size / 1024) . " KB";
                break;
            }
            default: {
                echo 'Zły format!';
                return false;
            }
        }
        return $result;
    }
}
