<?php

require 'File.php';

class ImageFile extends File
{
    private $ext;
    private $height;
    private $width;


    public function getExt()
    {
        return $this->ext;
    }

    public function setExt($ext)
    {
        if (strlen($ext) < 4){
            $this->ext = $ext;
        } else {
            die("złe rozszerzenie");
        }
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        if (is_numeric($height)) {
            $this->height = $height;
        } else {
            die("Niepoprawna wartość wysokości");
        }

    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width)
    {
        if (is_numeric($width)) {
            $this->width = $width;
        } else {
            die("Niepoprawna wartość szerokości");
        }
    }
    /**
     * Metoda zwiększająca wartość szerokości i wysyokości
     */
    public function resize($scale)
    {
        $this->width *= $scale;
        $this->height *= $scale;
    }
    /**
     *Metoda obliczająca ile pikseli mieści 1kb obrazka
     */
    public function pxpkb()
    {
        $size = $this->calculateSize('KB');
        $substracted = $this->width * $this->height;
        $pxpkb = $substracted / $size;

        return $pxpkb;
    }
}
