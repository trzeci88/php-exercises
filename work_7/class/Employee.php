<?php
/**
 * Created by PhpStorm.
 * User: Domino
 * Date: 19.10.2018
 * Time: 15:22
 */
class Employee {
    private $id;
    private $name;
    private $surname;
    private $salary;

    public function __construct($id, $name, $surname, $salary)
    {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->salary = $salary;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    public function raiseSalary($percent)
    {
        if (is_numeric($percent) && $percent > 0) {
            $percent = $percent / 100;
            $raise = $this->salary * $percent;
            $this->setSalary($this->salary + $raise);
        }
    }
}