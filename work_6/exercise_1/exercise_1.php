<?php
// Stworzenie tabel
//    $sql = "
//        CREATE TABLE Orders(id int AUTO_INCREMENT, description varchar(255), PRIMARY KEY(id));
//        CREATE TABLE Clients(id int AUTO_INCREMENT, name varchar(255), surname varchar(255), PRIMARY KEY(id))
//        CREATE TABLE Products (id int AUTO_INCREMENT, name varchar(255), description varchar(255), price decimal(5,2), PRIMARY KEY(id))";
//    $conn->query($sql);
//    echo "Tabela została utworzona";


$host ="localhost";
$user ="root";
$pass ="coderslab";
$db ="products_ex";
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!empty($_POST['name']) && !empty($_POST['description']) && !empty($_POST['price'])) {
        $name = $_POST['name'];
        $description = $_POST['description'];
        $price = $_POST['price'];
        try {
            $conn = new PDO("mysql:host=$host;dbname=$db;charset=utf8", $user, $pass);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);// Ustawienie obsługi błędów przez wyjątki
            $sql = "INSERT INTO Products(name, description, price) VALUES (:name, :description, :price)";
            $stmt = $conn->prepare($sql);
            $stmt->execute(['name'=>$name, 'description'=>$description, 'price'=>$price]);
            echo "Produkt został dodany";
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    } else {
        die("Nie wszystkie parametry zostały przesłane");
    }
} else {
    die("Dane nie zostały przesłane metodą POST");
}
