<?php

require_once '../../db_connect.php';

try {
    $conn->beginTransaction();

    $result = $conn->query("UPDATE pln_acc SET bal=bal-199 WHERE bal>=199");
    if ($result->numRows() > 0) {
        $conn->query("UPDATE eur_acc SET bal=bal+50");
        $conn->commit();
    } else {
        $conn->rollBack();
    }
} catch (PDOException $e) {
    $conn->rollBack();
    echo("Błąd: " . $e->getMessage());
}