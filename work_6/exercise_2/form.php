<?php
/**
 * Created by PhpStorm.
 * User: Domino
 * Date: 17.10.2018
 * Time: 20:43
 */

try {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (!empty($_POST['submit'])) {

            require_once '../../db_connect.php';
            $submit = $_POST['submit'];

            switch ($submit) {
                case 'cinemas':
                    $cinema_name = $_POST['cinema_name'];
                    $address = $_POST['address'];
                    if (!empty($cinema_name) && !empty($address)) {
                        $sql = "INSERT INTO cinemas(name, address) VALUES(:name, :address)";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute(['name'=>$cinema_name, 'address'=>$address]);
                        echo "Kino zostało dodane";
                    } else {
                        echo "Nie wypełniono wszystkich pół formularza";
                    }
                    break;
                case 'movies':
                    $movie_name = $_POST['movie_name'];
                    $description = $_POST['description'];
                    $rating = $_POST['rating'];
                    if (!empty($movie_name) && !empty($description) && !empty($rating)) {
                        if ($rating > 0) {
                            $sql = "INSERT INTO movies(name, description, rating) VALUES(:name, :description, :rating)";
                            $stmt = $conn->prepare($sql);
                            $stmt->execute(['name'=>$movie_name, 'description'=>$description, 'rating'=>$rating]);
                            echo "Film został dodany";
                        } else {
                            die("Ocena musi być większ od 0");
                        }
                    } else {
                        echo "Nie wypełniono wszystkich pół formularza";
                    }
                    break;
                case 'tickets':
                    $quantity = intval($_POST['quantity']);
                    $price = intval($_POST['price']);
                    if (!empty($quantity) && !empty($price)) {
                        if ($price > 0) {
                            $sql = "INSERT INTO tickets(quantity, price) VALUES(:quantity, :price)";
                            $stmt = $conn->prepare($sql);
                            $stmt->execute(['quantity'=>$quantity, 'price'=>$price]);
                            echo "Bilet został dodany";
                        } else {
                            die("Ocena musi być większ od 0");
                        }

                    } else {
                        echo "Nie wypełniono wszystkich pół formularza";
                    }
                    break;
                case 'payments':
                    $payment_type = $_POST['type'];
                    $date = $_POST['date'];
                    if (!empty($payment_type) && !empty($price)) {
                        $sql = "INSERT INTO payments(type, date) VALUES(:type, :date)";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute(['type'=>$payment_type, 'date'=>$date]);
                        echo "Płatność została dodana";
                    } else {
                        echo "Nie wypełniono wszystkich pół formularza";
                    }
                    break;
                default:
                    die("nie wciśnięto submit");
            }
        } else {
            die('Brak potwierdzenia');
        }
    } else {
        die('danych nie przesłano metodą post');
    }
} catch (PDOException $e) {
    echo $e->getMessage() . " Produkt nie został dodany";
}
