<?php
/**
 * Created by PhpStorm.
 * User: Domino
 * Date: 18.10.2018
 * Time: 02:08
 */

require_once '../../db_connect.php';

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $name = $_GET['name'];
    if (!empty($name) && is_string($name))
    switch ($name) {
        case "movies":
            $movies = $conn->query("SELECT * FROM movies");
            if ($movies->rowCount() > 0) {
                $movies = $movies->fetch(PDO::FETCH_ASSOC);
                foreach ($movies as $movie) {
                    $description = $movie['description'];
                    if (strlen($description) > 20) {
                        $description = substr($description, 0, 20) . "...";
                    }
                    echo "Film: " . $movie['name'] . " Opis: " . $description . " Ocena: " .$movie['rating']."\n";
            }
            } else {
                die("Brak filmów do wyświetlenia");
            }
            break;
        case "cinemas":
            $cinemas = $conn->query("SELECT * FROM cinemas");
            $cinemas = $cinemas->fetch(PDO::FETCH_ASSOC);
            foreach ($cinemas as $cinema) {
                echo "Kino: " . $cinema['name'] . " Adres: " . $cinema['address']."\n";
            }
            break;
        default:
            die("Błąd");

    }
} else {
    die("Dane nie zostały przesłane metodą GET");
}