<?php

class Calculator
{
    private $history;

    public function __construct()
    {
        $this->history = [];
    }

    public function add($num1, $num2)
    {
        return $this->saveToMemory('Added', $num1, $num2, $num1 + $num2);
    }

    public function multiply($num1, $num2)
    {
        return $this->saveToMemory('Multiplied', $num1, $num2, $num1 * $num2);
    }

    public function subtract($num1, $num2)
    {
        return $this->saveToMemory('Substracted', $num1, $num2, $num1 - $num2);
    }

    public function divide($num1, $num2)
    {
        if ($num2 == 0) {
            return false;
        }

        return $this->saveToMemory('Divided', $num1, $num2, $num1 / $num2);
    }

    private function saveToMemory($operation, $num1, $num2, $result)
    {
        $string = "$operation $num1 to $num2 got $result";
        $this->history[] = $string;

        return $result;
    }

    public function printOperations()
    {
        for ($i = 0; $i < count($this->history); $i++) {
            echo "Operacja " . ($i + 1) . ': ' . $this->history[$i] . "<br>";
        }
    }

    public function clearOperations()
    {
        $this->history = [];
    }
}
