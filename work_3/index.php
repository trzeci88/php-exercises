<?php
require_once 'Calculator.php';

$calc = new Calculator();

$calc->add(1, 4);
$calc->divide(8, 2);
$calc->multiply(4, 2);
$calc->printOperations();
$calc->clearOperations();
