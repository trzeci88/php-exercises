<?php
require('vendor/autoload.php');
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!empty($_POST['arrival']) && !empty($_POST['departure']) && !empty($_POST['date']) && !empty($_POST['hours']) && !empty($_POST['price'])) {

        $arrival = $_POST['arrival'];
        $departure = $_POST['departure'];
        $form_date = $_POST['date'];
        $hours = $_POST['hours'];
        $price = $_POST['price'];

        //Sprawdzenie czy miejsca przylotu i odlotu są różne
        if ($arrival === $departure) {
            die("Wybrane lotniska są takie same");
        }
        //Sprawdzenie czy czas lotu zostały wpisane
        if ($hours <= 0) {
            die("błędnie wpisane dane");
        }
        //Sprawdzenie czy cena została podana w prawidłowy sposób
        if ($price <= 0) {
            echo "Błędnie wprowadzona cena";
        }

// Wyciągnięcie ostatniego elementu z tablicy (timezone)
        $exploded_arrival = explode(' ', $arrival);
        $exploded_departure = explode(' ', $departure);
        $tz_arrival = array_pop($exploded_arrival);
        $tz_departure = array_pop($exploded_departure);

        $date = new DateTime($form_date);
// Nastawienie strefy czasowej wylotu
        $tz = new DateTimeZone($tz_departure);
        $set_tz = $date->setTimezone($tz);
        $format_date = $date->format("d-m-Y H:i:s");
//Nastawienie strefy czasowej przylotu
        $tz1 = new DateTimeZone($tz_arrival);
        $change_tz = $date->setTimezone($tz1);
        $format_date2 = $date->format("d-m-Y H:i:s");
// Dodanie czasu lotu
        $add_hours = $date->modify('+' . $hours . 'hours');
        $format_date3 = $date->format("d-m-Y H:i:s");

    } else {
        die("Jedno z pól formularza nie zostało wypełnione");
    }
} else {
    die("dane nie zostały wysłane metodą post");
}

//wygenerowanie losowego imienia i nazwiska
$faker = Faker\Factory::create();
$random_name = $faker->name;

use NumberToWords\NumberToWords;
$number_to_words = new NumberToWords();
$currency_transformer = $number_to_words->getCurrencyTransformer('pl');
$price_in_words = $currency_transformer->toWords($price * 100, 'PLN');

$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML("
    <h3>Dane pasażera</h3>
    <br>
    <p>Pasażer: $random_name </p>
    <br>
    <h3>Cena biletu</h3>
    <br>
    <p>Cena to: $price($price_in_words)</p>
    <br>
    <h3>Czas lotu</h3>
    <br>
    <p>Czas trwania lotu to: $hours godziny</p>
    <br>
    <h3>Dane wylotu</h3>
    <br>
    <p>Lotnisko: $departure <br><br>Data i czas wylotu (local time): $format_date</p>
    <br>
    <h3>Dane przylotu</h3>
    <br>
    <p>Lotnisko: $arrival <br><br>Data i czas przylotu (local time): $format_date2</p>");
$mpdf->Output();
?>

