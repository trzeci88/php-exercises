<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Flights</title>
</head>
<body>
    <form action="pdf.php" method="post" role="form">

        Lotnisko wylotu :
        <select name="departure">
            <?php
            require_once 'includes/airports.php';
            foreach ($airports as $names) {
                echo "<option>" . $names['name'] . " " . $names['timezone'] . "</option>";
            }
            ?>
        </select>

        Lotnisko przylotu :
        <select name="arrival"> 1
            <?php
            foreach ($airports as $names) {
                echo "<option>" . $names['name'] . " " . $names['timezone'] . "</option>";
            }
            ?>
        </select><br>

        Czas startu :
        <input type="datetime-local" placeholder="YYYY-MM-DD H:M:S" name="date"> </input><br>

        Długość lotu w godzinach :
        <input type="number" name="hours" min="0" step="1"> </input><br>

        Cena :
        <input type="number" name="price" min="0" step="0.01"> </input><br>
        <button type="submit" onclick="window.location='pdf.php'"> Wyślij</button>
    </form>
</body>
</html>
